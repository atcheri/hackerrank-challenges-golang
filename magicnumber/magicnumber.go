package magicnumber

import (
	"math"
	"sort"
)

type int32Slice []int32

func (f int32Slice) Len() int {
	return len(f)
}

func (f int32Slice) Less(i, j int) bool {
	return f[i] < f[j]
}

func (f int32Slice) Swap(i, j int) {
	f[i], f[j] = f[j], f[i]
}

// Square is a 2D int32 slice
type Square [][]int32

var magicSquares = []Square{

	{
		{8, 1, 6},
		{3, 5, 7},
		{4, 9, 2},
	},
	{
		{4, 3, 8},
		{9, 5, 1},
		{2, 7, 6},
	},
	{
		{2, 9, 4},
		{7, 5, 3},
		{6, 1, 8},
	},
	{
		{6, 7, 2},
		{1, 5, 9},
		{8, 3, 4},
	},
	{
		{6, 1, 8},
		{7, 5, 3},
		{2, 9, 4},
	},
	{
		{8, 3, 4},
		{1, 5, 9},
		{6, 7, 2},
	},
	{
		{4, 9, 2},
		{3, 5, 7},
		{8, 1, 6},
	},
	{
		{2, 7, 6},
		{9, 5, 1},
		{4, 3, 8},
	},
}

// func formingMagicSquare(s [][]int32) int32 {
// 	var cost int32
// 	if len(s) == 0 {
// 		return 0
// 	}
// 	cost = magicSquareCost(s)
// 	return cost
// }

func magicSumBySize(n int32) int32 {
	return n * int32(math.Pow(float64(n), 2)+1) / 2
}

func magicSquareCost(square Square) int32 {
	var costs []int32
	for _, ms := range magicSquares {
		cost := calculateCost(square, ms)
		if cost == 0 {
			return 0
		}
		costs = append(costs, cost)
	}
	sort.Sort(int32Slice(costs))
	return costs[0]
}

// func magicSquareCost(square Square) int32 {
// 	var cost float64
// 	var size int32 = int32(len(square))
// 	magicSum := magicSumBySize(size)
// 	for _, row := range square {
// 		cost += math.Abs(float64(magicSum) - float64(calculateSum(row)))
// 	}
// 	return int32(cost)
// }

// func magicSquareCosts(square Square) []int32 {
// 	var costs []int32
// 	var modifiedSquare Square
// 	if isMagicSquare(modifiedSquare) {
// 		cost := calculateCost(square, modifiedSquare)
// 		costs = append(costs, cost)
// 	}
// 	sort.Sort(int32Slice(costs))
// 	return costs
// }

func calculateCost(originalSquare Square, newSquare Square) int32 {
	var diff int32
	for i, row := range originalSquare {
		for j := range row {
			diff += int32(math.Abs(float64(originalSquare[i][j]) - float64(newSquare[i][j])))
		}
	}
	return diff
}

func isMagicSquare(square Square) bool {
	rows := convertToRows(square)
	uniqSums := make(map[int32]struct{})
	for _, row := range rows {
		sum := calculateSum(row)
		uniqSums[sum] = struct{}{}
	}
	return len(uniqSums) == 1
}

func convertToRows(square Square) [][]int32 {
	var rows [][]int32
	for _, row := range square {
		rows = append(rows, row)
	}
	transposed := transpose(square)
	for _, row := range transposed {
		rows = append(rows, row)
	}
	diagonals := extractDiagonals(square)
	for _, diag := range diagonals {
		rows = append(rows, diag)
	}
	return rows
}

func extractDiagonals(square Square) [][]int32 {
	size := len(square)
	diagonals := make([][]int32, 2)
	for i := range diagonals {
		diagonals[i] = make([]int32, size)
	}
	for i := 0; i < 2; i++ {
		for j := 0; j < len(square); j++ {
			if i == 0 {
				diagonals[i][j] = square[j][j]
			} else {
				diagonals[i][j] = square[j][size-j-1]
			}
		}
	}
	return diagonals
}

func calculateSum(row []int32) int32 {
	var sum int32 = 0
	for _, v := range row {
		sum += v
	}
	return sum
}

func transpose(slice [][]int32) [][]int32 {
	xl := len(slice[0])
	yl := len(slice)
	result := make([][]int32, xl)
	for i := range result {
		result[i] = make([]int32, yl)
	}
	for i := 0; i < xl; i++ {
		for j := 0; j < yl; j++ {
			result[i][j] = slice[j][i]
		}
	}
	return result
}
