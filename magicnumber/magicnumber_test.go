package magicnumber

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExtractDiagonals(t *testing.T) {
	input := Square{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
	}

	diagonals := extractDiagonals(input)

	assert.Len(t, diagonals, 2)
	assert.EqualValues(t, diagonals[0], []int32{1, 5, 9})
	assert.EqualValues(t, diagonals[1], []int32{3, 5, 7})
}

func TestCalculateSum(t *testing.T) {
	input := []int32{1, 2, 3, 4, 5, 6, 7, 8, 9}

	sum := calculateSum(input)
	assert.EqualValues(t, 45, sum)
}

func TestTranspose(t *testing.T) {
	input := Square{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
	}

	transposed := transpose(input)

	assert.EqualValues(t, [][]int32{
		{1, 4, 7},
		{2, 5, 8},
		{3, 6, 9},
	}, transposed)
}

func TestConvertToRows(t *testing.T) {
	input := Square{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
	}

	rows := convertToRows(input)

	assert.Len(t, rows, 8)
	assert.EqualValues(t, [][]int32{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
		{1, 4, 7},
		{2, 5, 8},
		{3, 6, 9},
		{1, 5, 9},
		{3, 5, 7},
	}, rows)
}

func TestIsMagicSquareFalse(t *testing.T) {
	input := Square{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
	}

	isMagic := isMagicSquare(input)

	assert.False(t, isMagic)
}

func TestIsMagicSquareTrue(t *testing.T) {
	input := Square{
		{8, 3, 4},
		{1, 5, 9},
		{6, 7, 2},
	}

	isMagic := isMagicSquare(input)

	assert.True(t, isMagic)
}

func TestCalculateCostSampleZero(t *testing.T) {
	original := Square{
		{4, 9, 2},
		{3, 5, 7},
		{8, 1, 5},
	}
	modified := Square{
		{4, 9, 2},
		{3, 5, 7},
		{8, 1, 6},
	}
	cost := calculateCost(original, modified)

	assert.EqualValues(t, 1, cost)
}

func TestCalculateCostSampleOne(t *testing.T) {
	original := Square{
		{4, 8, 2},
		{4, 5, 7},
		{6, 1, 6},
	}
	modified := Square{
		{4, 9, 2},
		{3, 5, 7},
		{8, 1, 6},
	}
	cost := calculateCost(original, modified)

	assert.EqualValues(t, 4, cost)
}

func TestCalculateCostSampleExample(t *testing.T) {
	original := Square{
		{5, 3, 4},
		{1, 5, 8},
		{6, 4, 2},
	}
	modified := Square{
		{8, 3, 4},
		{1, 5, 9},
		{6, 7, 2},
	}
	cost := calculateCost(original, modified)

	assert.EqualValues(t, 7, cost)
}

type testCase struct {
	input    Square
	expected int32
}

func TestMagicSquareCost(t *testing.T) {
	cases := []testCase{
		{
			input:    magicSquares[0],
			expected: 0,
		},
		{
			input: Square{
				{5, 3, 4},
				{1, 5, 8},
				{6, 4, 2},
			},
			expected: 7,
		},
		{
			input: Square{
				{4, 8, 2},
				{4, 5, 7},
				{6, 1, 6},
			},
			expected: 4,
		},
		{
			input: Square{
				{4, 9, 2},
				{3, 5, 7},
				{8, 1, 5},
			}, expected: 1,
		},
		{
			input: Square{
				{4, 5, 8},
				{2, 4, 1},
				{1, 9, 7},
			}, expected: 14,
		},
	}

	for _, testCase := range cases {
		cost := magicSquareCost(testCase.input)
		assert.EqualValues(t, testCase.expected, cost)
	}

}

func TestMagicSumBySize(t *testing.T) {
	assert.EqualValues(t, 15, magicSumBySize(3))
	assert.EqualValues(t, 34, magicSumBySize(4))
	assert.EqualValues(t, 65, magicSumBySize(5))
	assert.EqualValues(t, 175, magicSumBySize(7))
}
