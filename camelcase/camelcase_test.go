package camelcase

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExplodeCamelCaseStringNoUppercaseFirst(t *testing.T) {
	sentence := "Test"

	res, err := ExplodeCamelCaseString(sentence)
	assert.Nil(t, res)
	assert.NotNil(t, err)
	assert.EqualValues(t, startsWithUppercaseErrMsg, err.Error())
}

func TestExplodeCamelCaseStringSentenceHigherThanLimit(t *testing.T) {
	sentence := "testSentenceTooLong"
	SizeLimit = 10

	res, err := ExplodeCamelCaseString(sentence)

	assert.Nil(t, res)
	assert.NotNil(t, err)
	assert.EqualValues(t, sizeExceededErrMsg, err.Error())
}

func TestExplodeCamelCaseStringSentenceLowerThanLimit(t *testing.T) {
	sentence := "testCorrectSize"
	SizeLimit = 20

	_, err := ExplodeCamelCaseString(sentence)

	assert.Nil(t, err)
}

func TestExplodeCamelCaseStringWithValidSentence(t *testing.T) {
	sentence := "testSentence"

	res, err := ExplodeCamelCaseString(sentence)
	assert.Nil(t, err)
	assert.NotNil(t, res)
	assert.Len(t, res, 2)
	assert.EqualValues(t, "test", res[0])
	assert.EqualValues(t, "sentence", res[len(res)-1])

}
