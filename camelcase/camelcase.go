package camelcase

import (
	"errors"
	"fmt"
	"math"
	"strings"
	"unicode"
)

var (
	sentence = "thisIsATestSentenceWrittenByAlice"

	startsWithUppercaseErrMsg = "The Sentence start with a capital letter"
	sizeExceededErrMsg        = "The Sentence is too long"

	// SizeLimit is the size limit of a setence in
	SizeLimit = int(math.Pow10(5))
)

func init() {
	fmt.Println("Alice writing camelcased sentences")
}

// ExplodeCamelCaseString ...
func ExplodeCamelCaseString(sentence string) ([]string, error) {
	if len(sentence) >= SizeLimit {
		return nil, errors.New(sizeExceededErrMsg)
	}
	runes := []rune(sentence)
	if unicode.IsUpper(runes[0]) {
		return nil, errors.New(startsWithUppercaseErrMsg)
	}
	var words []string
	var word string
	var s string
	for i, r := range runes {
		s = string(r)
		if unicode.IsLower(r) {
			word += s
		}
		if unicode.IsUpper(r) || i == len(runes)-1 {
			words = append(words, strings.ToLower(word))
			word = s
		}
	}
	return words, nil
}
