package averybigsum

func Sum(ar []int64, limit int, max float64) int64 {
	if len(ar) > limit {
		panic("The size of the input is greater than the defined limit")
	}
	var sum int64
	for _, v := range ar {
		if v > int64(max) {
			panic("The value of one of the inputs is greater than the defined max")
		}
		sum += v
	}
	return sum
}
