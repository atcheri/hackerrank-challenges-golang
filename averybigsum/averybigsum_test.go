package averybigsum

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSumTooManyInput(t *testing.T) {
	// arrange
	maxSize := 3
	input := []int64{1, 2, 3, 4}

	// assert
	assert.Panics(t, func() {
		// act
		Sum(input, maxSize, float64(10))
	})
}

func TestSumTooLargeInput(t *testing.T) {
	// arrange
	maxSize := 10
	input := []int64{1, 2, 3, 4}
	max := 3

	// assert
	assert.Panics(t, func() {
		// act
		Sum(input, maxSize, float64(max))
	})
}

func TestSumSampleZero(t *testing.T) {
	// arrange
	maxSize := 10
	input := []int64{1000000001, 1000000002, 1000000003, 1000000004, 1000000005}
	max := math.Pow10(10)

	// act
	sum := Sum(input, maxSize, max)

	// assert
	assert.EqualValues(t, 5000000015, sum)
}
