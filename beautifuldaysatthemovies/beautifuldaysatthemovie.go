package beautifuldaysatthemovie

import (
	"math"
	"strconv"
)

func canBeDividedBy(n int32, d int32) bool {
	if d == 0 {
		panic("impossible operation: dividing by zero")
	}
	return n%d == 0
}

func diffWithReverse(n int32) int32 {
	str := strconv.Itoa(int(n))
	reversed := reverseString(str)
	m, _ := strconv.Atoi(reversed)

	return int32(math.Abs(float64(int(n) - m)))
}

func reverseString(str string) string {
	reversed := ""
	for _, v := range str {
		reversed = string(v) + reversed
	}
	return reversed
}

func beautifulDays(i int32, j int32, k int32) int32 {
	var days int32 = 0
	for num := i; num <= j; num++ {
		if canBeDividedBy(diffWithReverse(num), k) {
			days++
		}
	}
	return days
}
