package beautifuldaysatthemovie

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDiffWithReverse(t *testing.T) {
	assert.EqualValues(t, 0, diffWithReverse(5))
	assert.EqualValues(t, 18, diffWithReverse(20))
	assert.EqualValues(t, 0, diffWithReverse(22))
	assert.EqualValues(t, 27, diffWithReverse(36))
	assert.EqualValues(t, 27, diffWithReverse(63))
}

func TestCanBeDividedBy(t *testing.T) {
	assert.False(t, canBeDividedBy(20, 6))
	assert.False(t, canBeDividedBy(21, 6))
	assert.False(t, canBeDividedBy(22, 6))
	assert.False(t, canBeDividedBy(23, 6))
	assert.True(t, canBeDividedBy(24, 6))

	assert.False(t, canBeDividedBy(5, 2))

	assert.Panics(t, func() {
		canBeDividedBy(10, 0)
	})
}

func TestBeautifulDays(t *testing.T) {
	assert.EqualValues(t, 2, beautifulDays(20, 23, 6))
}
