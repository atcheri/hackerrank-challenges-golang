package ceasarcipher

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCipherEmpty(t *testing.T) {
	// arrange
	original := ""
	var offset int32 = 0

	// act
	ciphered := cipher(original, offset)

	// assert
	assert.EqualValues(t, "", ciphered)
}

func TestCipherFirstAlphabetLetter(t *testing.T) {
	// arrange
	original := "a"
	var offset int32 = 3

	// act
	ciphered := cipher(original, offset)

	// assert
	assert.EqualValues(t, "d", ciphered)
}

func TestCipherLastAlphabetLetter(t *testing.T) {
	// arrange
	original := "z"
	var offset int32 = 3

	// act
	ciphered := cipher(original, offset)

	// assert
	assert.EqualValues(t, "c", ciphered)
}

func TestCipherComplexWord(t *testing.T) {
	// arrange
	original := "abcdefijkxyz"
	var offset int32 = 3

	// act
	ciphered := cipher(original, offset)

	// assert
	assert.EqualValues(t, "defghilmnabc", ciphered)
}

func TestCipherSentence(t *testing.T) {
	// arrange
	original := "abc efg xyz."
	var offset int32 = 3

	// act
	ciphered := cipher(original, offset)

	// assert
	assert.EqualValues(t, "def hij abc.", ciphered)
}

func TestCipherUppercases(t *testing.T) {
	// arrange
	original := "ABC XYZ"
	var offset int32 = 3

	// act
	ciphered := cipher(original, offset)

	// assert
	assert.EqualValues(t, "DEF ABC", ciphered)
}

func TestCipherHarkerRankExample(t *testing.T) {
	// arrange
	original := "middle-Outz"
	var offset int32 = 2

	// act
	ciphered := cipher(original, offset)

	// assert
	assert.EqualValues(t, "okffng-Qwvb", ciphered)
}

func TestCipherTooLongSentence(t *testing.T) {
	// arrange
	SentenceSizeLimit = 5
	original := "abcdef"
	var offset int32 = 3

	// act

	// assert
	assert.Panics(t, func() {
		cipher(original, offset)
	})
}
