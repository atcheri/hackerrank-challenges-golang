package ceasarcipher

import (
	"fmt"
	"unicode"
)

var (
	// SentenceSizeLimit ...
	SentenceSizeLimit      = 100
	lowerA            rune = 'a'
	lowerZ            rune = 'z'
	uppreA            rune = 'A'
	uppreZ            rune = 'Z'
	blankSpace        rune = ' '
	point             rune = '.'
	hyphen            rune = '-'
	underscore        rune = '_'
)

func cipher(original string, offset int32) string {
	if original == "" {
		return ""
	}
	runes := []rune(original)
	if len(runes) > SentenceSizeLimit {
		panic(fmt.Sprintf("The sentence is too long, max size = %d", SentenceSizeLimit))
	}
	var cipheredRunes []rune
	var offsettedRune, startLetter, endLetter rune

	for _, r := range runes {
		offsettedRune = r
		startLetter = lowerA
		endLetter = lowerZ
		if !unicode.IsLetter(r) {
			offsettedRune = r
		} else {
			if unicode.IsUpper(r) {
				startLetter = uppreA
				endLetter = uppreZ
			}
			var ratio int32 = (int32(r) + offset) / endLetter
			if ratio == 0 {
				offsettedRune += offset
			} else {
				offsettedRune = startLetter + r + offset - endLetter - 1
			}
		}
		cipheredRunes = append(cipheredRunes, offsettedRune)
	}
	ciphered := string(cipheredRunes)
	return ciphered
}
