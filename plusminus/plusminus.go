package plusminus

import "fmt"

func CalculateRatios(arr []int32) []string {
	var plus, minus, zeros uint32
	for _, v := range arr {
		switch {
		case v > 0:
			plus++
		case v < 0:
			minus++
		case v == 0:
			zeros++
		}
	}
	positiveRatio := float32(plus) / float32(len(arr))
	negativeRatio := float32(minus) / float32(len(arr))
	zeroRatio := float32(zeros) / float32(len(arr))

	return []string{
		fmt.Sprintf("%.6f", positiveRatio),
		fmt.Sprintf("%.6f", negativeRatio),
		fmt.Sprintf("%.6f", zeroRatio),
	}
}
