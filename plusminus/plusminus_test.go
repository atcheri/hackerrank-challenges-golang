package plusminus

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCalculateRatiosSampleZero(t *testing.T) {
	input := []int32{-4, 3, -9, 0, 4, 1}

	ret := CalculateRatios(input)

	// assert
	expected := []string{"0.500000", "0.333333", "0.166667"}
	assert.EqualValues(t, expected, ret)
}
