package staircase

import "fmt"

func PrintStaircase(n int32) {
	var i, j, max int32
	max = n - 1
	for i = 0; i < n; i++ {
		for j = 0; j < n; j++ {
			c := "#"
			if j < max {
				c = " "
			}
			fmt.Printf("%s", c)
		}
		max--
		fmt.Print("\n")
	}
}
