package pickingnumbers

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUniqNumbers(t *testing.T) {
	numbers := []int32{1, 5, 7, 8, 9, 1, 15, 9, 4, 5, 8, 9, 15, 14, 13, 15, 16}

	uniqs := uniqNumbers(numbers)

	assert.Len(t, uniqs, 10)
}

func TestBuildCombinations(t *testing.T) {
	uniq := []int32{1, 4, 5, 7, 8, 9, 13, 14, 15, 16}

	combinations := buildCombinations(uniq)

	assert.Len(t, combinations, 6)
}

func TestCountOccurrences(t *testing.T) {
	numbers := []int32{1, 5, 7, 8, 9, 1, 15, 9, 4, 5, 8, 9, 15, 14, 13, 15, 16}
	c := combination{a: 4, b: 5}

	count := countOccurrences(numbers, c)

	assert.EqualValues(t, 3, count)
}

func TestMaxOccurrenceCustom(t *testing.T) {
	numbers := []int32{1, 5, 7, 8, 9, 1, 15, 9, 4, 5, 8, 9, 15, 14, 13, 15, 16}

	count := maxOccurrence(numbers)

	assert.EqualValues(t, 5, count)
}

func TestMaxOccurrenceSample7(t *testing.T) {
	numbers := []int32{4, 97, 5, 97, 97, 4, 97, 4, 97, 97, 97, 97, 4, 4, 5, 5, 97, 5, 97, 99, 4, 97, 5, 97, 97, 97, 5, 5, 97, 4, 5, 97, 97, 5, 97, 4, 97, 5, 4, 4, 97, 5, 5, 5, 4, 97, 97, 4, 97, 5, 4, 4, 97, 97, 97, 5, 5, 97, 4, 97, 97, 5, 4, 97, 97, 4, 97, 97, 97, 5, 4, 4, 97, 4, 4, 97, 5, 97, 97, 97, 97, 4, 97, 5, 97, 5, 4, 97, 4, 5, 97, 97, 5, 97, 5, 97, 5, 97, 97, 97}

	count := maxOccurrence(numbers)

	assert.EqualValues(t, 50, count)
}
