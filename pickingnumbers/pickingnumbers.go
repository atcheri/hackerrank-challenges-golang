package pickingnumbers

import (
	"math"
	"sort"
)

type combination struct {
	a int32
	b int32
}

type int32Slice []int32

func (f int32Slice) Len() int {
	return len(f)
}

func (f int32Slice) Less(i, j int) bool {
	return f[i] < f[j]
}

func (f int32Slice) Swap(i, j int) {
	f[i], f[j] = f[j], f[i]
}

func maxOccurrence(numbers []int32) int32 {
	combinations := buildCombinations(uniqNumbers(numbers))
	if len(combinations) < 1 {
		return 0
	}
	var newMax int32
	max := countOccurrences(numbers, combinations[0])
	for i := 1; i < len(combinations); i++ {
		newMax = countOccurrences(numbers, combinations[i])
		if newMax > max {
			max = newMax
		}
	}
	return max
}

func uniqNumbers(numbers []int32) []int32 {
	if len(numbers) < 1 {
		return []int32{}
	}
	uniqs := make(int32Slice, 0)
	uniqMap := make(map[int32]struct{})
	for _, n := range numbers {
		if _, ok := uniqMap[n]; ok == false {
			uniqMap[n] = struct{}{}
			uniqs = append(uniqs, n)
		}
	}

	sort.Sort(uniqs)
	return uniqs
}

func buildCombinations(uniqs []int32) []combination {
	combinations := make([]combination, 0)
	for i := 0; i < len(uniqs)-1; i++ {
		diff := math.Abs(float64(uniqs[i]) - float64(uniqs[i+1]))
		if diff == 1 {
			combinations = append(combinations, combination{a: uniqs[i], b: uniqs[i+1]})
		}
	}
	return combinations
}

func countOccurrences(numbers []int32, c combination) int32 {
	var counter int32 = 0
	for _, n := range numbers {
		if n == c.a || n == c.b {
			counter++
		}
	}
	return counter
}
