package birthdaycandles

import "sort"

type int32Slice []int32

func (f int32Slice) Len() int {
	return len(f)
}

func (f int32Slice) Less(i, j int) bool {
	return f[i] < f[j]
}

func (f int32Slice) Swap(i, j int) {
	f[i], f[j] = f[j], f[i]
}

func Candles(ar []int32) int32 {
	converted := int32Slice(ar)
	sort.Sort(converted)
	max := converted[len(ar)-1]
	var count int32 = 0
	for _, v := range ar {
		if v >= max {
			count++
		}
	}
	return count
}
