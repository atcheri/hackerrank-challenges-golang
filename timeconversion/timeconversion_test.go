package timeconversion

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConvertToMilitaryFormat(t *testing.T) {
	input := "07:05:45PM"

	output := ConvertToMilitaryFormat(input)

	assert.EqualValues(t, "19:05:45", output)
}
