package timeconversion

import (
	"time"
)

func ConvertToMilitaryFormat(input string) string {
	t := ConvertToTime(input)
	return t.Format("15:04:05")
}

func ConvertToTime(input string) time.Time {
	t, err := time.Parse("15:04:05PM", input)
	if err != nil {
		panic("Time conversion failed")
	}
	return t
}
