package diagonaldifference

import "math"

func SumMatrixDiagonal(arr [][]int32) int32 {
	length := len(arr)
	var sum1, sum2 int32
	for i := 0; i < length; i++ {
		sum1 += arr[i][i]
		sum2 += arr[length-i-1][i]
	}

	return int32(math.Abs(float64(sum1) - float64(sum2)))
}
