package diagonaldifference

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSumMatrixDiagonalSampleZero(t *testing.T) {

	input := [][]int32{
		{11, 2, 4},
		{4, 5, 6},
		{10, 8, -12},
	}

	diff := SumMatrixDiagonal(input)

	assert.EqualValues(t, 15, diff)
}
