package hackerrankinastring

import (
	"sort"
	"strings"
)

func foundWord(word string, in string) bool {
	inRunes := []rune(in)
	indexes := make([]int, 0)
	var lastIndex, nextRuneIndex int
	for _, r := range inRunes {
		index := strings.IndexRune(word[lastIndex:], r)
		if index == -1 {
			return false
		}
		nextRuneIndex = index + lastIndex
		indexes = append(indexes, nextRuneIndex)
		lastIndex = nextRuneIndex + 1
	}

	return sort.IntsAreSorted(indexes)
}
