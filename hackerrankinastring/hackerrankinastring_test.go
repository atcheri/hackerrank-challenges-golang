package hackerrankinastring

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	hackerrank = "hackerrank"
)

func TestFoundWordSample0(t *testing.T) {
	// act
	first := foundWord("hereiamstackerrank", hackerrank)
	second := foundWord("hackerworld", hackerrank)

	// assert
	assert.True(t, first)
	assert.False(t, second)
}

func TestFoundWordSampleNumber(t *testing.T) {
	// act

	testMap := map[string]string{
		"10":                         "NO",
		"knarrekcah":                 "NO",
		"hackerrank":                 "YES",
		"hackeronek":                 "NO",
		"abcdefghijklmnopqrstuvwxyz": "NO",
		"rhackerank":                 "NO",
		"ahankercka":                 "NO",
		"hacakaeararanaka":           "YES",
		"hhhhaaaaackkkkerrrrrrrrank": "YES",
		"crackerhackerknar":          "NO",
		"hhhackkerbanker":            "NO",
	}
	for key, val := range testMap {
		ret := foundWord(key, hackerrank)
		// assert
		if val == "YES" {
			assert.True(t, ret)
		}
		if val == "NO" {
			assert.False(t, ret)
		}

	}

}
