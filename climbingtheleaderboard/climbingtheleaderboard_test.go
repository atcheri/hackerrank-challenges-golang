package climbingtheleaderboard

import (
	"bufio"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSliceIndex(t *testing.T) {
	slice := []int32{100, 100, 50, 40, 40, 20, 10}
	index, err := sliceIndex(slice, 50)
	assert.Nil(t, err)
	assert.EqualValues(t, 2, index)

	index, err = sliceIndex(slice, 500)
	assert.NotNil(t, err)
	assert.EqualValues(t, -1, index)
}

func TestFindRank(t *testing.T) {
	tests := []struct {
		in  int32
		out int32
		msg string
	}{
		{
			in:  5,
			out: 6,
			msg: "in 5 out 6",
		},
		{
			in:  25,
			out: 4,
			msg: "in 25 out 4",
		},
		{
			in:  50,
			out: 2,
			msg: "in 50 out 2",
		},
		{
			in:  120,
			out: 1,
			msg: "in 120 out 1",
		},
	}

	var res int32
	scores := []int32{100, 100, 50, 40, 40, 20, 10}
	for _, test := range tests {
		res = findRankFromMap(scores, test.in)
		assert.EqualValues(t, test.out, res)
	}
}

func TestFindRankSample1(t *testing.T) {
	tests := []struct {
		in  int32
		out int32
		msg string
	}{
		{
			in:  50,
			out: 6,
		},
		{
			in:  65,
			out: 5,
		},
		{
			in:  77,
			out: 4,
		},
		{
			in:  90,
			out: 2,
		},
		{
			in:  102,
			out: 1,
		},
	}

	var res int32
	scores := []int32{100, 90, 90, 80, 75, 60}
	for _, test := range tests {
		res = findRankFromMap(scores, test.in)
		assert.EqualValues(t, test.out, res)
	}
}

func TestFindRankSample8(t *testing.T) {
	// input := readFile("./test_6_input.txt")
	// _ = input
	// assert.FailNow(t, "blzla")
}

func readFile(path string) []int32 {
	file, err := os.Open(path)
	check(err)
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()
	fmt.Println(scanner.Text())
	scanner.Scan()
	fmt.Println(scanner.Text())

	scores := make([]int32, 0)
	// for scanner.Scan() {
	// }

	if err := scanner.Err(); err != nil {
		check(err)
	}
	return scores

}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
