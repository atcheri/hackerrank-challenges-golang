package climbingtheleaderboard

import (
	"errors"
	"sort"
)

type int32Slice []int32

func (f int32Slice) Len() int {
	return len(f)
}

func (f int32Slice) Less(i, j int) bool {
	return f[i] < f[j]
}

func (f int32Slice) Swap(i, j int) {
	f[i], f[j] = f[j], f[i]
}

func scanRank(scores []int32, score int32) int32 {
	size := len(scores)
	if size < 2 {
		return 1
	}
	if score > scores[0] {
		return 1
	}
	var rank int32 = 1
	var prev int32 = scores[0]
	for i := 1; i < size; i++ {
		if prev == scores[i] {
			prev = scores[i]
			continue
		}
		// rank++
		if score >= scores[i] {
			return rank
		}
		rank++
	}
	return rank
}

func findRankFromMap(scores []int32, score int32) int32 {
	size := len(scores)
	if size < 2 {
		return 1
	}
	if score > scores[0] {
		return 1
	}
	scoreMap := make(map[int32]struct{})
	scoreMap[score] = struct{}{}
	for i := 0; i < size; i++ {
		if score > scores[i] {
			break
		}
		if _, ok := scoreMap[scores[i]]; ok == false {
			scoreMap[scores[i]] = struct{}{}
		}
	}
	sortedScores := make([]int32, len(scoreMap))
	ind := 0
	for k := range scoreMap {
		sortedScores[ind] = k
		ind++
	}
	sort.Sort(sort.Reverse(int32Slice(sortedScores)))
	return int32(len(sortedScores))
}

func findRank(scores []int32, score int32) int32 {
	leaderboard := buildLeaderboard(score, scores)
	index, err := sliceIndex(leaderboard, score)
	if err != nil {
		panic("The score doesn't fit anywhere in the leaderboard")
	}
	return index + 1
}

func buildLeaderboard(score int32, scores []int32) []int32 {
	newScores := make([]int32, 0)
	scores = append(scores, score)
	for _, s := range scores {
		if _, err := sliceIndex(newScores, s); err != nil {
			newScores = append(newScores, s)
		}
	}
	sort.Sort(sort.Reverse(int32Slice(newScores)))
	return newScores
}

func sliceIndex(slice []int32, searchedValue int32) (int32, error) {
	for i, v := range slice {
		if v == searchedValue {
			return int32(i), nil
		}
	}
	return -1, errors.New("Searched value not found in slice")
}
