package angryprofessor

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsClassPlanned(t *testing.T) {
	assert.True(t, isClassPlanned([]int32{-1, -1, 0, 0, 1, 1}, 4))
	assert.False(t, isClassPlanned([]int32{-1, -1, 0, 0, 1, 1}, 5))
	assert.False(t, isClassPlanned([]int32{-1, -3, 4, 2}, 3))
	assert.True(t, isClassPlanned([]int32{0, -1, 2, 1}, 2))
}

func TestCountStudentsOnTime_Example(t *testing.T) {
	arrivalTimes := []int32{-1, -1, 0, 0, 1, 1}

	count := countStudentsOnTime(arrivalTimes)

	assert.EqualValues(t, 4, count)
}

func TestCountStudentsOnTime_SampleZero(t *testing.T) {
	arrivalTimes := []int32{-1, -3, 4, 2}

	count := countStudentsOnTime(arrivalTimes)

	assert.EqualValues(t, 2, count)
}

func TestCountStudentsOnTime_SampleOne(t *testing.T) {
	arrivalTimes := []int32{0, -1, 2, 1}

	count := countStudentsOnTime(arrivalTimes)

	assert.EqualValues(t, 2, count)
}
