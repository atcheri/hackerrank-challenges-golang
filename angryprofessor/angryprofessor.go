package angryprofessor

func isClassPlanned(times []int32, min int32) bool {
	return countStudentsOnTime(times) >= min
}

func countStudentsOnTime(times []int32) int32 {
	count := 0
	for _, time := range times {
		if time <= 0 {
			count++
		}
	}
	return int32(count)
}
