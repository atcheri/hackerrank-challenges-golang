package pangrams

import (
	"errors"
	"math"
	"strings"
)

var (
	// StringSizeLimit ...
	stringSizeLimit = int(math.Pow10(3))
	// ExceededStringSizeErrMsg ...
	ExceededStringSizeErrMsg = "The sentence exceeds the lmit"
	// AlphabetLettersCount ...
	AlphabetLettersCount = 26
)

// IsPangrams ...
func IsPangrams(sentence string, limit int) bool {
	runes := []rune(strings.ReplaceAll(strings.ToLower(sentence), " ", ""))
	if len(runes) > limit {
		panic(errors.New(ExceededStringSizeErrMsg))
	}
	alphabetMap := make(map[string]struct{})
	for _, r := range runes {
		alphabetMap[string(r)] = struct{}{}
	}
	return len(alphabetMap) == AlphabetLettersCount
}
