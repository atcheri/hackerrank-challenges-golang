package pangrams

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsPangramsConstraints(t *testing.T) {
	// arrange
	sentence := "sentence is really too long."
	limit := 10

	// assert
	assert.PanicsWithError(t, ExceededStringSizeErrMsg, func() {
		// act
		IsPangrams(sentence, limit)
	})
}

func TestIsPangramsSampleZero(t *testing.T) {
	// arrange
	sentence := "We promptly judged antique ivory buckles for the next prize"
	limit := stringSizeLimit

	// act
	ret := IsPangrams(sentence, limit)

	// assert
	assert.True(t, ret)
}

func TestIsPangramsSampleOne(t *testing.T) {
	// arrange
	sentence := "We promptly judged antique ivory buckles for the prize"
	limit := stringSizeLimit

	// act
	ret := IsPangrams(sentence, limit)

	// assert
	assert.False(t, ret)
}
