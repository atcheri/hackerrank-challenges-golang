package minimaxsum

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMinMaxSumSampleZero(t *testing.T) {
	input := []int64{1, 2, 3, 4, 5}

	max, min := MinMaxSum(input)

	assert.GreaterOrEqual(t, max, min)
	assert.EqualValues(t, 14, max)
	assert.EqualValues(t, 10, min)
}

func TestMinMaxSumSampleOne(t *testing.T) {
	input := []int64{256741038, 623958417, 467905213, 714532089, 938071625}

	max, min := MinMaxSum(input)

	assert.GreaterOrEqual(t, max, min)
	assert.EqualValues(t, 2744467344, max)
	assert.EqualValues(t, 2063136757, min)
}
