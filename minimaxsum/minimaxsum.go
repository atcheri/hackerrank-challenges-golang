package minimaxsum

import (
	"math"
	"sort"
)

var (
	maxInt = int64(math.Pow10(9))
)

type int64Slice []int64

func (f int64Slice) Len() int {
	return len(f)
}

func (f int64Slice) Less(i, j int) bool {
	return f[i] < f[j]
}

func (f int64Slice) Swap(i, j int) {
	f[i], f[j] = f[j], f[i]
}

func MinMaxSum(arr []int64) (max int64, min int64) {
	sort.Sort(int64Slice(arr))
	return Sum(arr[1:]), Sum(arr[:len(arr)-1])
}

func Sum(input []int64) int64 {
	var res int64
	for _, v := range input {
		res += v
	}
	return res
}
