package marsexploration

import (
	"errors"
	"strings"
)

var (
	SOS                      = "SOS"
	MessageSizeLimit         = 99
	ExceedMaxSizeErrMsg      = "The message is bigger than the limit"
	NotMultipleOfThreeErrMsg = "The message cannot be divided by three"
	NotUppercaseErrMsg       = "The message is not uppercase"
)

// CountAlteredCharacters ...
func CountAlteredCharacters(msg string, limit int) int32 {
	runes := []rune(msg)
	length := len(runes)
	if strings.ToUpper(msg) != msg {
		panic(errors.New(NotUppercaseErrMsg))
	}
	if length > limit {
		panic(errors.New(ExceedMaxSizeErrMsg))
	}
	if length%3 != 0 {
		panic(errors.New(NotMultipleOfThreeErrMsg))
	}

	var count int32 = 0
	var extract []rune
	for i := 0; i < length; i += 3 {
		extract = runes[i : i+3]
		if string(extract) != SOS {
			count += CountDiff(extract)
		}
	}

	return count
}

func CountDiff(extract []rune) int32 {
	var ret int32 = 0
	sosRunes := []rune(SOS)
	for i, e := range extract {
		if e != sosRunes[i] {
			ret++
		}
	}
	return ret
}
