package marsexploration

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCountAlteredCharactersLength(t *testing.T) {
	// arrange
	sentence := "THIS IS A LONG SENTENCE."
	maxSize := 10

	// assert
	assert.PanicsWithError(t, ExceedMaxSizeErrMsg, func() {
		// act
		CountAlteredCharacters(sentence, maxSize)
	})
}

func TestCountAlteredCharactersMultipleOfThree(t *testing.T) {
	// arrange
	sentence := "THIS IS A SENTENCE THAT CANNOT BE DIVIDED BY THREE"

	// assert
	assert.PanicsWithError(t, NotMultipleOfThreeErrMsg, func() {
		// act
		CountAlteredCharacters(sentence, MessageSizeLimit)
	})
}

func TestCountAlteredCharactersLowercase(t *testing.T) {
	// arrange
	sentence := "This sentence contains lowercase"

	// assert
	assert.PanicsWithError(t, NotUppercaseErrMsg, func() {
		// act
		CountAlteredCharacters(sentence, MessageSizeLimit)
	})
}

func TestCountAlteredCharactersSampleZero(t *testing.T) {
	// arrange
	sentence := "SOSSPSSQSSOR"

	// act
	count := CountAlteredCharacters(sentence, MessageSizeLimit)

	// assert
	assert.EqualValues(t, 3, int(count))
}

func TestCountAlteredCharactersSampleOne(t *testing.T) {
	// arrange
	sentence := "SOSSOT"

	// act
	count := CountAlteredCharacters(sentence, MessageSizeLimit)

	// assert
	assert.EqualValues(t, 1, int(count))
}

func TestCountAlteredCharactersSampleTwo(t *testing.T) {
	// arrange
	sentence := "SOSSOSSOS"

	// act
	count := CountAlteredCharacters(sentence, MessageSizeLimit)

	// assert
	assert.EqualValues(t, 0, int(count))
}

func TestCountAlteredCharactersSampleThree(t *testing.T) {
	// arrange
	sentence := "SOSOOSOSOSOSOSSOSOSOSOSOSOS"

	// act
	count := CountAlteredCharacters(sentence, MessageSizeLimit)

	// assert
	assert.EqualValues(t, 12, int(count))
}
