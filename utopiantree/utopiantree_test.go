package utopiantree

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUtopianTree_Zero(t *testing.T) {
	height := utopianTree(0)
	assert.EqualValues(t, 1, height)
}

func TestUtopianTree_One(t *testing.T) {
	height := utopianTree(1)
	assert.EqualValues(t, 2, height)
}

func TestUtopianTree_Four(t *testing.T) {
	height := utopianTree(4)
	assert.EqualValues(t, 7, height)
}
