package utopiantree

func utopianTree(n int32) int32 {
	if n == 0 {
		return 1
	}
	height := 1
	for i := 1; i < int(n)+1; i++ {
		if i%2 == 0 {
			height++
		} else {
			height *= 2
		}
	}
	return int32(height)
}
