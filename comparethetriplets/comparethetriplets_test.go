package comparethetriplets

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPointsEmpty(t *testing.T) {
	// arrange
	alicePoints := make([]int32, 0)
	bobPoints := make([]int32, 0)

	// act
	ret := Points(alicePoints, bobPoints)

	// assert
	assert.Len(t, ret, 2)

}

func TestPointsSampleZero(t *testing.T) {
	// arrange
	alicePoints := []int32{5, 6, 7}
	bobPoints := []int32{3, 6, 10}

	// act
	ret := Points(alicePoints, bobPoints)

	// assert
	assert.NotEmpty(t, ret)
	assert.EqualValues(t, []int32{1, 1}, ret)

}

func TestPointsSampleOne(t *testing.T) {
	// arrange
	alicePoints := []int32{17, 28, 30}
	bobPoints := []int32{99, 16, 8}

	// act
	ret := Points(alicePoints, bobPoints)

	// assert
	assert.NotEmpty(t, ret)
	assert.EqualValues(t, []int32{2, 1}, ret)

}
