package comparethetriplets

// Points compares two slices of points and returns ...
func Points(a []int32, b []int32) []int32 {
	points := make([]int32, 2)
	if len(a) != len(b) {
		panic("The slices of points are not the same size")
	}
	// if len(a) == 0 || len(b) == 0 {
	// 	panic("There are no points to compare")
	// }
	for idx, pointA := range a {
		pointB := b[idx]
		switch {
		case pointA > pointB:
			points[0]++
		case pointA < pointB:
			points[1]++
		}
	}
	return points
}
